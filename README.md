# Django 2.0 - Aprendendo os Conceitos Fundamentais

Código referente ao curso Django 2.0 - Aprendendo os Conceitos Fundamentais da plataforma Udemy ministrado pelo Engenheiro/Arquiteto de Software Gregory Pacheco.

### Instalação do Python
```
    sudo apt-get install build-essential
    sudo apt-get install python3
```

### Instalação do Pip
```
    sudo apt-get install python3-pip
```
### Instalação de Django
```
    pip3 install django
```
### Instalação do Pycharm
```
    sudo snap install pycharm-community --classic
```
### Executando
```
    python3 manage.py runserver
```
#### Para atualizar o banco de dados do servidor, deve-se utilizar os comandos:
```
    python3 manage.py makemigrations
    python3 manage.py migrate
```

Sistema Operacional: Linux 4.15.0-44-generic #47-Ubuntu SMP Mon Jan 14 11:26:59 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux